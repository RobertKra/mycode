import requests

URL = "https://api.spacexdata.com/v3/capsules"


def main():
    spacedata = requests.get(URL)
    # requests.post()
    # requests.delete()
    # requests.put()

    # HTTP Transport Error checking
    if spacedata.status_code != 200:
        print(f"Uh-oh! It looks like a non-200 status code was returned :( The code was: {spacedata.status_code}")
        return

    spacedata_json = spacedata.json()

    for launch in spacedata_json:
        #print(launch, "\n")
        if launch.get("details"):
            print(f'We just launched {launch.get("capsule_serial")},{launch.get("details")}\n')
        else:
            print(f'We just launched {launch.get("capsule_serial")},{"I do not have details on this capsule launch"}\n')


if __name__ == "__main__":
    main()
