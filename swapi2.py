#!/usr/bin/env python3
"""Alta3 Research
   Star Wars API HTTP response parsing"""

# pprint makes dictionaries a lot more human readable
from pprint import pprint

# requests is used to send HTTP requests (get it?)
import requests

#URL = "https://swapi.dev/luke/force"      # Comment out this line
URL= "https://swapi.dev/api/people/4/"     # Uncomment this line
URL2 = "https://swapi.dev/api/films/1/"

def main():
    """sending GET request, checking response"""

    # SWAPI response is stored in "resp" object
    resp= requests.get(URL)

    # check to see if the status is anything other than what we want, a 200 OK
    if resp.status_code == 200:
        # convert the JSON content of the response into a python dictionary
        vader= resp.json()
        pprint(vader)
        print("{"+vader['name'] + "} was born in the year {"+vader['birth_year']+"}. His eyes are now {yellow} and his hair color is {none}.")

    else:
        print("That is not a valid URL.")
    resp= requests.get(URL2)
    if resp.status_code == 200:
        # convert the JSON content of the response into a python dictionary
        films= resp.json()
        pprint(films)
if __name__ == "__main__":
    main()

